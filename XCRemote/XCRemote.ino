#include <bluefruit.h>

BLEDis bledis;
BLEHidAdafruit blehid;

//pins
const int BUTTONS_PIN = A0;                                                   //measuring voltage of resistor ladder (which button is pressed)
const int ANYBUTTON_PIN = 3;                                                  //measuring if any button is pressed
const int OUTPUT_PIN = A3;                                                    //going high when any button is pressed
const int WAKE_PIN = 4;                                                       //for waking up from deep sleep
const int LED_PIN = 17;                                                       //blink LED
const int VBAT_PIN = A7;                                                      //measure battery voltage

//user settings
const char keys[] = {'1', '2', 'd', 'r', 'u', 'l', '3', '4'};                 //which keys to send
char repeatKeys[] = {'d', 'r', 'u', 'l', '2', '3'};                           //those keys will be sent repeatedly with 'keyRepeatInterval', all others will be sent once (and can be short- and long-pressed therefor)
const int powerkey = 3;                                                       //which key is used to confirm power on (see numbers on PCB)
unsigned long powerDownAfter = 600;                                           //minutes before power down after last key press
unsigned long powerDownAfterUnconnected = 1;                                  //minutes before power down after last connected to device
const int keyRepeatIntervalMax = 500;                                         //ms after which a pressed key is repeated initially
const int keyRepeatIntervalMin = 100;                                         //ms after which a pressed key is repeated at the end of the logarithmic decelaration
const float keyRepeatAcceleration = 0.90;                                     //1 = no acceleration, < 1 = more acceleration (key repetitions are sent faster over time)                                         
const bool serialOutput = false;                                              //disable for power saving!
const bool calibrationMode = false;                                           //enable calibration mode, you must first disconnect from any BLE-connection
const int buttonVoltages[] = {3310, 3020, 2740, 2450, 2145, 1795, 1360, 790}; //values of each step on resistor ladder

//advanced settings (don't change unless you know why & how)
const int tolerance = 10;                                                     //tolerance for analog readings to still be counted, otherwise new measurement cycle is started (too much noise on ADC)
int myButtonDividerValues[8];                                                 //values of each center between resistor ladder values as divider/gate
const int omitButtonMinLevel = 3700;                                          //any reading higher than this is considered false (button bouncing f.e.)
const int led_blink_short = 80;                                               //short LED blink interval
const int led_blink_long = 300;                                               //long LED blink interval
const int averagingAnalogReads = 100;                                         //how many readings to take and average of analog in

//initial values
volatile int pinValue = 0;                                                    //current measurement on analog pin
int divider = 0;                                                              //default divider
volatile bool btnDown = false;                                                //is currently any button pressed
volatile bool btnUp = true;                                                   //are all buttons unpressed
volatile bool btnUpSent = true;                                               //is last button up already sent
volatile char currentKey;                                                     //currently pressed key
volatile char lastKey;                                                        //last sent key
unsigned long lastButtonPress = 0;                                            //store millis() of last button press
unsigned long lastButtonChange = 0;                                            //store millis() of last button change
unsigned long lastConnected = 0;                                              //store millis() of last connection to a device
volatile int pinValueToCalibrate = 0;                                         //raw value of returned voltage to calibrate a new board
int keyRepeatIntervalDiff = keyRepeatIntervalMax - keyRepeatIntervalMin;      //this div will get exponentially lower for acceleration

boolean elementInArray(char array[], char element);
boolean elementInArray(char array[], char element) {
  for (int i = 0; i < 99; i++) {
    if (array[i] == element) {
      return true;
    }
  }
  return false;
}

void setup(){
  if(serialOutput){
    Serial.begin(115200); Serial.println(); Serial.println("hello");  
  }
  setDividerValues();
  powerDownAfter = powerDownAfter * 1000 * 60; //needs value in ms, is configured in m
  powerDownAfterUnconnected = powerDownAfterUnconnected * 1000 * 60; //needs value in ms, is configured in m
  Bluefruit.begin();
  Bluefruit.setTxPower(0); // -40 draws 0.06mA less than 4
  Bluefruit.setName("XCRemote");
  Bluefruit.autoConnLed(false);
  //configure and start device information service
  bledis.setManufacturer("Raphael Jeger / Adafruit Industries");
  bledis.setModel("Bluefruit Feather 52");
  bledis.begin();
  //start human interface device service
  blehid.begin();
  startAdv();
  //set pin modes, resolutions and interrupts
  pinMode(BUTTONS_PIN, INPUT);
  pinMode(ANYBUTTON_PIN, INPUT_PULLUP);
  pinMode(OUTPUT_PIN, OUTPUT);
  digitalWrite(OUTPUT_PIN, LOW);
  pinMode(LED_PIN, OUTPUT);
  digitalWrite(LED_PIN, LOW);
  analogReadResolution(12);
  attachInterrupt(digitalPinToInterrupt(ANYBUTTON_PIN), btnPress, FALLING); //when any button is pressed
  nrf_gpio_cfg_sense_input(g_ADigitalPinMap[WAKE_PIN], NRF_GPIO_PIN_PULLUP, NRF_GPIO_PIN_SENSE_LOW); //wake from deep sleep
  //set lowest power modes
  sd_power_mode_set(NRF_POWER_MODE_LOWPWR);
  sd_power_dcdc_mode_set(NRF_POWER_DCDC_ENABLE);  
  if(!serialOutput){
    Serial.end();
  }
  checkPowerUp();
}

void loop(){
  if(Bluefruit.connected() && (btnDown || !btnUpSent)){ //device is connected via BLE and either a btn is pressed or button up is not yet sent
      if((millis() - lastButtonPress > (keyRepeatIntervalMin + keyRepeatIntervalDiff) || lastKey != currentKey) && btnDown && currentKey != NULL){ //send button down at key repeat interval or if another key is pressed
        lastButtonPress = millis();
        keyRepeatIntervalDiff = pow(keyRepeatIntervalDiff, keyRepeatAcceleration);
        sendKeyPress();
      }else if(!btnUpSent && currentKey != NULL){ //send button up only once
        if(elementInArray(repeatKeys, currentKey)){ //repeatedly send auto-repeating keys f.e. for panning, zooming
          sendKeyRelease();
        }else if(!elementInArray(repeatKeys, currentKey) && btnUp){ //send those keys only when button is up, making short / long presses possible
          sendKeyRelease(); 
        }
      }
  }else{
    delay(100);
    if(calibrationMode){
      writeSerial("pin value: "+String(pinValueToCalibrate)+" btnDown: "+String(btnDown)+" btnUp: "+String(btnUp));
    }
    if(Bluefruit.connected() && !calibrationMode){ //connected to a device and not in calibration mode, power down later
      lastConnected = millis();
      if(millis() - lastButtonPress > powerDownAfter){
        writeSerial("power down because no keypress for "+String(powerDownAfter/1000)+" seconds");
        powerDown();
      }
    }else if(!calibrationMode){ //not connected to a device and not in calibration mode, power down early
      if(millis() - lastConnected > powerDownAfterUnconnected){
        writeSerial("power down because not connected for "+String(powerDownAfterUnconnected/1000)+" seconds");
        powerDown();
      }
    }
  }
}

void sendKeyPress() {
  blehid.keyPress(currentKey);
  writeSerial("key down "+String(currentKey));
  lastKey = currentKey;
  btnUpSent = false;
}

void sendKeyRelease() {
  blehid.keyRelease();
  writeSerial("key up "+String(currentKey));
  btnUpSent = true;
}

void btnPress(){ //ISR called by ANYBUTTON_PIN falling
  if(millis() - lastButtonChange > 20){
    lastButtonChange = millis();
    digitalWrite(OUTPUT_PIN, HIGH);
    getKey();
    digitalWrite(OUTPUT_PIN, LOW);
    attachInterrupt(digitalPinToInterrupt(ANYBUTTON_PIN), btnRelease, RISING); //when any button is released  
  }
}

void btnRelease(){ //ISR called by ANYBUTTON_PIN rising
  if(millis() - lastButtonChange > 20){
    lastButtonChange = millis();
    btnDown = false;
    btnUp = true;
    keyRepeatIntervalDiff = keyRepeatIntervalMax - keyRepeatIntervalMin; //next button must start "slow" again
    attachInterrupt(digitalPinToInterrupt(ANYBUTTON_PIN), btnPress, FALLING); //when any button is pressed
  }
}

bool getKey(){ //analogue reading of voltage-level and mapping to key
  volatile int lastPinValue = 0;
  volatile int currentPinValue = 0;
  for (int measurement = 0; measurement < averagingAnalogReads; measurement++) { //let's calculate the average of several readings, because ADCs are noisy
    currentPinValue = analogRead(BUTTONS_PIN);
    if (abs(lastPinValue - currentPinValue) > tolerance || currentPinValue > omitButtonMinLevel) { //too much noise on the ADC, let's start the measuring cycle again
      measurement = 0;
      pinValue = 0;
    } else { //in tolerances, count that measurement
      pinValue += currentPinValue;
    }
    lastPinValue = currentPinValue;
  }
  pinValue = pinValue / averagingAnalogReads;
  if(calibrationMode){
    pinValueToCalibrate = pinValue;
  }
  //some key is pressed, iterate to find out which one
  for (int i = 7; i >= 0; i--) {
    if (pinValue < myButtonDividerValues[i]) {
      currentKey = keys[i];
      btnDown = true;
      btnUp = false;
      return true;
    }
  }
}

void checkPowerUp(){
  if(!calibrationMode){
    int matches = 0;
    int timeout = 10000;
    while(true){
      if (millis() >= timeout){
        writeSerial("boot not ok");
        powerDown();
      }
      if(currentKey == keys[powerkey-1]){ //some key has been pressed, continue to boot
        analogReference(AR_INTERNAL_3_0);
        float measuredvbat = analogRead(VBAT_PIN) * 4.2 / 4096;
        analogReference(AR_DEFAULT); 
        writeSerial("VBat: " + String(measuredvbat));
        if(measuredvbat > 4.1){ //battery is full
          blinkLED(LED_PIN, led_blink_long, 3);
        }else if(measuredvbat > 3.7){ //battery still above nominal
          blinkLED(LED_PIN, led_blink_long, 2);
        }else if(measuredvbat > 3.2){ //battery still usable
          blinkLED(LED_PIN, led_blink_long, 1);
        }else{ //battery is too low, switch off
          blinkLED(LED_PIN, led_blink_short, 10);
          powerDown();
        }
        break;
      }
    }
  }
}

void powerDown(){
  writeSerial("goodbye");
  blinkLED(LED_PIN, led_blink_short, 10);
  sd_power_system_off();
}

void blinkLED(uint32_t LED_PIN, long interval, int cycles){
  int ledState = LOW;
  unsigned long previousMillis = 0;
  int i = 0;
  while(true){
    if(i == cycles*2) break;
    unsigned long currentMillis = millis();
    if (currentMillis - previousMillis >= interval) {
      previousMillis = currentMillis;
      if (ledState == LOW) {
        ledState = HIGH;
      } else {
        ledState = LOW;
      }
      digitalWrite(LED_PIN, ledState);
      i++;
    }
  }
}

void startAdv(void){  
  // Advertising packet
  Bluefruit.Advertising.addFlags(BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE);
  Bluefruit.Advertising.addTxPower();
  Bluefruit.Advertising.addAppearance(BLE_APPEARANCE_HID_KEYBOARD);
  
  // Include BLE HID service
  Bluefruit.Advertising.addService(blehid);

  // There is enough room for the dev name in the advertising packet
  Bluefruit.Advertising.addName();
  
  Bluefruit.Advertising.restartOnDisconnect(true);
  Bluefruit.Advertising.setInterval(32, 1024);    // in unit of 0.625 ms
  Bluefruit.Advertising.setFastTimeout(30);       // number of seconds in fast mode
  Bluefruit.Advertising.start(60);                // 0 = Don't stop advertising after n seconds
}

void setDividerValues(){
  for (int i = 0; i < sizeof(myButtonDividerValues) / sizeof(myButtonDividerValues[0]); i++) { //find the divider between each button analog value as a logic gate
    if (i == 0) {
      //first, so no left to get divider, so special treatment
      divider = (omitButtonMinLevel + buttonVoltages[i]) / 2;
    } else {
      //read divider between this and the one before
      divider = (buttonVoltages[i] + buttonVoltages[i - 1]) / 2;
    }
    myButtonDividerValues[i] = divider;
  }
}

void writeSerial(String string){
  if(serialOutput){
    Serial.println(string);
  }
}
