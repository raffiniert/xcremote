EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Switch:SW_SPST SW7
U 1 1 5FDCD228
P 3640 1370
F 0 "SW7" V 3630 1360 50  0000 R CNN
F 1 "SW_SPST" V 3595 1282 50  0001 R CNN
F 2 "Button_Switch_THT:SW_PUSH-12mm" H 3640 1370 50  0001 C CNN
F 3 "~" H 3640 1370 50  0001 C CNN
	1    3640 1370
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_SPST SW6
U 1 1 5FDCD73E
P 3940 1370
F 0 "SW6" V 3940 1350 50  0000 R CNN
F 1 "SW_SPST" V 3895 1282 50  0001 R CNN
F 2 "Button_Switch_THT:SW_PUSH-12mm" H 3940 1370 50  0001 C CNN
F 3 "~" H 3940 1370 50  0001 C CNN
	1    3940 1370
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_SPST SW5
U 1 1 5FDCDB96
P 4240 1370
F 0 "SW5" V 4240 1360 50  0000 R CNN
F 1 "SW_SPST" V 4195 1282 50  0001 R CNN
F 2 "Button_Switch_THT:SW_PUSH-12mm" H 4240 1370 50  0001 C CNN
F 3 "~" H 4240 1370 50  0001 C CNN
	1    4240 1370
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_SPST SW4
U 1 1 5FDCFE47
P 4540 1370
F 0 "SW4" V 4540 1370 50  0000 R CNN
F 1 "SW_SPST" V 4495 1282 50  0001 R CNN
F 2 "Button_Switch_THT:SW_PUSH-12mm" H 4540 1370 50  0001 C CNN
F 3 "~" H 4540 1370 50  0001 C CNN
	1    4540 1370
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_SPST SW3
U 1 1 5FDCFE4D
P 4840 1370
F 0 "SW3" V 4840 1340 50  0000 R CNN
F 1 "SW_SPST" V 4795 1282 50  0001 R CNN
F 2 "Button_Switch_THT:SW_PUSH-12mm" H 4840 1370 50  0001 C CNN
F 3 "~" H 4840 1370 50  0001 C CNN
	1    4840 1370
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_SPST SW2
U 1 1 5FDCFE53
P 5140 1370
F 0 "SW2" V 5140 1360 50  0000 R CNN
F 1 "SW_SPST" V 5095 1282 50  0001 R CNN
F 2 "Button_Switch_THT:SW_PUSH-12mm" H 5140 1370 50  0001 C CNN
F 3 "~" H 5140 1370 50  0001 C CNN
	1    5140 1370
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_SPST SW1
U 1 1 5FDCFE59
P 5440 1370
F 0 "SW1" V 5440 1360 50  0000 R CNN
F 1 "SW_SPST" V 5395 1282 50  0001 R CNN
F 2 "Button_Switch_THT:SW_PUSH-12mm" H 5440 1370 50  0001 C CNN
F 3 "~" H 5440 1370 50  0001 C CNN
	1    5440 1370
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R7
U 1 1 5FDD39FE
P 3790 1170
F 0 "R7" H 3790 1283 50  0000 C CNN
F 1 "1k" H 3790 1081 50  0001 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 3720 1170 50  0001 C CNN
F 3 "~" H 3790 1170 50  0001 C CNN
	1    3790 1170
	1    0    0    -1  
$EndComp
$Comp
L Device:R R6
U 1 1 5FDD4196
P 4090 1170
F 0 "R6" H 4090 1283 50  0000 C CNN
F 1 "1k" H 4090 1081 50  0001 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 4020 1170 50  0001 C CNN
F 3 "~" H 4090 1170 50  0001 C CNN
	1    4090 1170
	1    0    0    -1  
$EndComp
$Comp
L Device:R R5
U 1 1 5FDD45AE
P 4390 1170
F 0 "R5" H 4390 1283 50  0000 C CNN
F 1 "1k" H 4390 1081 50  0001 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 4320 1170 50  0001 C CNN
F 3 "~" H 4390 1170 50  0001 C CNN
	1    4390 1170
	1    0    0    -1  
$EndComp
$Comp
L Device:R R4
U 1 1 5FDD49C4
P 4690 1170
F 0 "R4" H 4690 1283 50  0000 C CNN
F 1 "1k" H 4690 1081 50  0001 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 4620 1170 50  0001 C CNN
F 3 "~" H 4690 1170 50  0001 C CNN
	1    4690 1170
	1    0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 5FDD4D46
P 4990 1170
F 0 "R3" H 4990 1283 50  0000 C CNN
F 1 "1k" H 4990 1081 50  0001 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 4920 1170 50  0001 C CNN
F 3 "~" H 4990 1170 50  0001 C CNN
	1    4990 1170
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 5FDD51F4
P 5290 1170
F 0 "R2" H 5290 1283 50  0000 C CNN
F 1 "1k" H 5290 1081 50  0001 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 5220 1170 50  0001 C CNN
F 3 "~" H 5290 1170 50  0001 C CNN
	1    5290 1170
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 5FDD568E
P 5590 1170
F 0 "R1" H 5590 1283 50  0000 C CNN
F 1 "1k" H 5590 1081 50  0001 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 5520 1170 50  0001 C CNN
F 3 "~" H 5590 1170 50  0001 C CNN
	1    5590 1170
	1    0    0    -1  
$EndComp
Connection ~ 3940 1170
Connection ~ 4240 1170
Connection ~ 4540 1170
Connection ~ 4840 1170
Connection ~ 5140 1170
Connection ~ 5440 1170
Connection ~ 3940 1570
Wire Wire Line
	3940 1570 4240 1570
Connection ~ 4240 1570
Wire Wire Line
	4840 1570 5140 1570
Connection ~ 5140 1570
Wire Wire Line
	5140 1570 5440 1570
Connection ~ 5440 1570
$Comp
L Connector:Conn_01x05_Male J1
U 1 1 5FDDB168
P 2300 1440
F 0 "J1" H 2272 1418 50  0000 R CNN
F 1 "Conn_01x05_Male" H 2408 1730 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x05_P2.54mm_Vertical" H 2300 1440 50  0001 C CNN
F 3 "~" H 2300 1440 50  0001 C CNN
	1    2300 1440
	-1   0    0    1   
$EndComp
Text Label 3040 1170 2    50   ~ 0
G
Text Label 2100 1640 2    50   ~ 0
G
Text Label 2100 1440 2    50   ~ 0
A1
Text Label 2100 1340 2    50   ~ 0
A2
Text Label 2100 1240 2    50   ~ 0
A3
Text Label 5740 1170 0    50   ~ 0
A3
Wire Wire Line
	5740 1570 5740 1480
Wire Wire Line
	5740 1480 5790 1480
Wire Wire Line
	5740 1570 5790 1570
Connection ~ 5740 1570
Wire Wire Line
	5740 1570 5740 1660
Wire Wire Line
	5740 1660 5790 1660
Text Label 2100 1540 2    50   ~ 0
A0
Text Label 5790 1480 0    50   ~ 0
A0
Text Label 5790 1570 0    50   ~ 0
A1
Text Label 5790 1660 0    50   ~ 0
A2
Wire Notes Line
	1890 1040 2510 1040
Wire Notes Line
	2510 1040 2510 1770
Wire Notes Line
	2510 1770 1890 1770
Wire Notes Line
	1890 1770 1890 1040
Text Notes 2390 1920 2    50   ~ 10
CONNECTOR
Wire Wire Line
	5440 1570 5740 1570
Connection ~ 4840 1570
Connection ~ 4540 1570
Wire Wire Line
	4540 1570 4840 1570
Wire Wire Line
	4240 1570 4540 1570
Connection ~ 3640 1170
$Comp
L Device:R R8
U 1 1 5FDD301E
P 3490 1170
F 0 "R8" H 3490 1283 50  0000 C CNN
F 1 "1k" H 3490 1081 50  0001 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 3420 1170 50  0001 C CNN
F 3 "~" H 3490 1170 50  0001 C CNN
	1    3490 1170
	1    0    0    -1  
$EndComp
Text Label 3290 1080 2    50   ~ 10
A
Text Label 3650 1050 2    50   ~ 10
B
Text Label 3940 1040 2    50   ~ 10
C
Text Label 4240 1050 2    50   ~ 10
D
Text Label 4540 1050 2    50   ~ 10
E
Text Label 4830 1050 2    50   ~ 10
F
Text Label 5130 1060 2    50   ~ 10
GG
Text Label 5430 1060 2    50   ~ 10
H
$Comp
L Switch:SW_SPST SW8
U 1 1 5FDCC293
P 3340 1370
F 0 "SW8" V 3340 1600 50  0000 R CNN
F 1 "SW_SPST" V 3295 1282 50  0001 R CNN
F 2 "Button_Switch_THT:SW_PUSH-12mm" H 3340 1370 50  0001 C CNN
F 3 "~" H 3340 1370 50  0001 C CNN
	1    3340 1370
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R9
U 1 1 5FDEF053
P 3190 1170
F 0 "R9" H 3190 1070 50  0000 L CNN
F 1 "1k" H 3190 1081 50  0001 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 3120 1170 50  0001 C CNN
F 3 "~" H 3190 1170 50  0001 C CNN
	1    3190 1170
	-1   0    0    1   
$EndComp
Connection ~ 3340 1170
Wire Wire Line
	3340 1570 3640 1570
Connection ~ 3640 1570
Wire Wire Line
	3640 1570 3940 1570
Text Label 2310 2470 0    50   ~ 0
G
Wire Notes Line
	1930 2070 2880 2070
Wire Notes Line
	2880 2070 2880 2750
Wire Notes Line
	2880 2750 1880 2750
Wire Notes Line
	1880 2750 1880 2070
Wire Notes Line
	1880 2070 1920 2070
Wire Notes Line
	6040 900  6040 1800
Wire Notes Line
	6040 1800 2870 1800
Wire Notes Line
	2870 1800 2870 900 
Wire Notes Line
	2870 900  6040 900 
Text Notes 2190 2860 0    50   ~ 0
BATTERY
$Comp
L Connector:Conn_01x03_Male J3
U 1 1 5FDF6879
P 2660 2390
F 0 "J3" H 2632 2368 50  0000 R CNN
F 1 "Conn_01x03_Male" H 2632 2413 50  0001 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 2660 2390 50  0001 C CNN
F 3 "~" H 2660 2390 50  0001 C CNN
	1    2660 2390
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2460 2290 2310 2290
$Comp
L Connector_Generic:Conn_01x01 J2
U 1 1 5FE0F94A
P 2110 2290
F 0 "J2" H 2120 2180 50  0000 C CNN
F 1 "Conn_01x01" H 2028 2156 50  0001 C CNN
F 2 "TestPoint:TestPoint_Pad_2.0x2.0mm" H 2110 2290 50  0001 C CNN
F 3 "~" H 2110 2290 50  0001 C CNN
	1    2110 2290
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J4
U 1 1 5FE10872
P 2110 2470
F 0 "J4" H 2090 2590 50  0000 C CNN
F 1 "Conn_01x01" H 2028 2336 50  0001 C CNN
F 2 "TestPoint:TestPoint_Pad_2.0x2.0mm" H 2110 2470 50  0001 C CNN
F 3 "~" H 2110 2470 50  0001 C CNN
	1    2110 2470
	-1   0    0    1   
$EndComp
$EndSCHEMATC
